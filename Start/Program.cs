﻿namespace Start
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;

    class Program
    {
        enum Command
        {
            START_PROCESS,
            OPEN_FILE,
            OPEN_WINDOWS_EXPLORER
        }

        static void Main(string[] args)
        {
            bool error = false;
            List<string> errorMessages = new List<string>();

            string initializationFile = Directory.GetCurrentDirectory() + @"\Start.ini";

            string directoryName = Path.GetDirectoryName(initializationFile);

            if (!Directory.Exists(directoryName))
            {
                errorMessages.Add($"Error could not find directory'{directoryName}'");
                error = true;
                DisplayErrorsAndWaitForInput(errorMessages);
                return;
            }

            if (!File.Exists(initializationFile))
            {
                errorMessages.Add($"Error could not find file'{initializationFile}'");
                error = true;
                DisplayErrorsAndWaitForInput(errorMessages);
                return;
            }

            string[] lines = File.ReadAllLines(initializationFile);

            foreach (string line in lines)
            {
                string localLine = RemoveDuplicateTabCharacters(line);

                string[] parts = localLine.Split(separator: new char[] { '\t' }, options: StringSplitOptions.RemoveEmptyEntries);

                if (parts.Length < 1)
                    continue;

                string command = parts[0].ToUpper();

                if (command == Command.START_PROCESS.ToString())
                {
                    ValidateLine(line, parts, ref errorMessages, ref error, expectedNumParts: 3);
                    if (error)
                        continue;

                    string processName = parts[1];
                    string processPath = parts[2];

                    bool success = StartProcessIfNotAlreadyRunning(processName, processPath);

                    if (!success)
                    {
                        errorMessages.Add($"Error processing process name '{processName}' with process path '{processPath}'");
                        error = true;
                    }
                }

                else if (command == Command.OPEN_FILE.ToString())
                {
                    ValidateLine(line, parts, ref errorMessages, ref error, expectedNumParts: 2);
                    if (error)
                        continue;

                    string filename = parts[1];
                    filename = filename.Replace("\"", string.Empty);

                    bool success = OpenFileIfNotAlreadyOpened(filename);

                    if (!success)
                    {
                        errorMessages.Add($"Error opening file '{filename}'");
                        error = true;
                    }
                }

                else if (command == Command.OPEN_WINDOWS_EXPLORER.ToString())
                {
                    ValidateLine(line, parts, ref errorMessages, ref error, expectedNumParts: 2);
                    if (error)
                        continue;

                    string directoryPath = parts[1];
                    directoryPath = directoryPath.Replace("\"", string.Empty);

                    try
                    {
                        Process.Start(directoryPath);
                    }
                    catch (Exception)
                    {
                        errorMessages.Add($"Error getting directory path '{directoryPath}'");
                        error = true;
                    }
                }
            }

            if (error)
            {
                DisplayErrorsAndWaitForInput(errorMessages);
            }
        }

        private static void DisplayErrorsAndWaitForInput(List<string> errorMessages)
        {
            foreach (string errorMessage in errorMessages)
                Console.WriteLine(errorMessage);

            Console.Write("\nErrors ocurred. Hit <ENTER> to exit");
            Console.ReadLine();
        }

        private static void ValidateLine(string line, string[] parts, ref List<string> errorMessages, ref bool error, byte expectedNumParts)
        {
            if (parts.Length != expectedNumParts)
            {
                errorMessages.Add($"Error. Expected '{expectedNumParts}' parts for '{line}'. Aborting this command.");
                error = true;
            }
        }

        private static string RemoveDuplicateTabCharacters(string line)
        {
            do
            {
                string newValue = line.Replace("\t\t", "\t");
                if (newValue == line)
                    break;
                line = newValue;
            } while (true);

            return line;
        }

        private static bool OpenFileIfNotAlreadyOpened(string wholePath)
        {
            string path = Path.GetDirectoryName(wholePath);
            string filename = Path.GetFileName(wholePath);

            if (!Directory.Exists(path))
                return false;

            if (!File.Exists(wholePath))
                return false;

            try
            {
                FileStream fileStream = File.Open(path: wholePath, mode: FileMode.Open);
                fileStream.Close();
                StartProcessIfNotAlreadyRunning(processName: "", processPath: wholePath);

                return true;
            }
            catch (FileNotFoundException fnfex)
            {
                return false;
                throw fnfex;
            }
            catch (IOException)
            {
                // If the file is already open, don't throw an exception
                // I'm not sure if what else will throw this exception

                return true;
            }
        }

        private static bool StartProcessIfNotAlreadyRunning(string processName, string processPath)
        {
            Process[] processes = Process.GetProcessesByName(processName);

            try
            {
                if (processes.Length == 0)
                    Process.Start(processPath);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
